import { PollsService } from '../resource'
import { parseDateFromServer } from '../utils/date'
import { ResponseRecognitionError } from '../utils/error'

const ACTIONS = {
  SET_POLLS: 'SET_POLLS',
  SET_ERROR: 'SET_ERROR',
  SET_LOADING: 'SET_LOADING'
}

const questionsModule = {
  namespaced: true,
  state: {
    polls: [],
    error: null,
    isLoading: true
  },
  mutations: {
    [ACTIONS.SET_POLLS](state, polls) {
      state.polls = polls
      state.isLoading = false
      state.error = null
    },
    [ACTIONS.SET_ERROR](state, error) {
      state.isLoading = false
      state.error = error
    },
    [ACTIONS.SET_LOADING](state) {
      state.isLoading = true
    }
  },
  actions: {
    async loadPolls ({ commit, state }) {
      commit(ACTIONS.SET_LOADING)
      let result;
      try {
        result = await PollsService.getPolls()
      } catch (serverError) {
        commit(ACTIONS.SET_ERROR, serverError)
        return
      }
      try {
        const polls = result.data
        const datedPolls = polls.map(poll => {
          const { date_begin, date_end, ...pollWithoutDates } = poll;
          return {
            ...pollWithoutDates,
            dateBegin: parseDateFromServer(poll.date_begin),
            dateEnd: parseDateFromServer(poll.date_end)
          }
        })
        datedPolls.sort((p1, p2) => p1.title.localeCompare(p2.title))
        commit(ACTIONS.SET_POLLS, datedPolls)
      } catch (responseRecognitionError) {
        commit(ACTIONS.SET_ERROR, new ResponseRecognitionError(responseRecognitionError))
      }
    }
  },
  getters: {
    getPolls (state) {
      return state.polls
    },
    isLoading (state) {
      return state.isLoading
    },
    getError (state) {
      return state.error
    }
  }
}

export default questionsModule
