import { PollsService } from '../resource'
import { formatDateForServer, parseDateFromServer } from '../utils/date'
import { ResponseRecognitionError } from '../utils/error';

function clearQuestions (questions) {
  return questions.map(question => ({
    ...question,
    ...question.isVerbal
      ? {
        answers: [],
        verbal: question.verbal || {}
      } : {
        answers: question.answers || [],
        verbal: {}
      }
  }))
}

function migrateQuestionsToPoll(questionsOrPoll) {
  if (questionsOrPoll.questions) {
    return questionsOrPoll;
  }
  return {
    questions: questionsOrPoll
  }
}

const ACTIONS = {
  SET_LOADING: 'SET_LOADING',
  SET_ERROR: 'SET_ERROR',
  SET_POLL: 'SET_POLL',
  CREATE_POLL: 'CREATE_POLL'
}

const pollModule = {
  namespaced: true,
  state: {
    isLoading: false,
    poll: null,
    error: null,
    isNew: false
  },
  mutations: {
    [ACTIONS.SET_LOADING](state) {
      state.isLoading = true
    },
    [ACTIONS.SET_ERROR](state, error) {
      state.isLoading = false
      state.error = error
    },
    [ACTIONS.SET_POLL](state, poll) {
      state.isLoading = false
      state.poll = poll
      state.error = false
      state.isNew = false
    },
    [ACTIONS.CREATE_POLL](state) {
      state.isLoading = false
      state.poll = {
        title: '',
        dateBegin: null,
        dateEnd: null,
        data: {
          questions: []
        }
      }
      state.error = false
      state.isNew = true
    }
  },
  actions: {
    async loadPoll ({ commit, state }, id) {
      commit(ACTIONS.SET_LOADING)
      let result;
      try {
        result = await PollsService.loadPoll(id)
      } catch (serverError) {
        commit(ACTIONS.SET_ERROR, serverError)
        return
      }
      try {
        const resultData = result.data;
        const pollDataOrQuestions = resultData.data && JSON.parse(resultData.data) || [];
        const pollData = migrateQuestionsToPoll(pollDataOrQuestions);
        const pollDataClearedQuestions = {
          questions: clearQuestions(pollData.questions)
        }
        const poll = {
          id: resultData.id,
          title: resultData.title,
          dateBegin: parseDateFromServer(resultData.date_begin),
          dateEnd: parseDateFromServer(resultData.date_end),
          data: pollDataClearedQuestions
        }
        commit(ACTIONS.SET_POLL, poll)
      } catch (responseRecognitionError) {
        commit(ACTIONS.SET_ERROR, new ResponseRecognitionError(responseRecognitionError))
      }
    },
    async savePoll ({ commit, state }) {
      commit(ACTIONS.SET_LOADING)
      const pollData = state.poll.data;
      const pollDataClearedQuestions = {
        questions: clearQuestions(pollData.questions)
      }
      let doSave;
      if (state.isNew) {
        const saveResultData = {
          title: state.poll.title,
          description: '<blank>',
          date_begin: formatDateForServer(state.poll.dateBegin),
          date_end: formatDateForServer(state.poll.dateEnd),
          data: JSON.stringify(pollDataClearedQuestions)
        }
        doSave = PollsService.createPoll(saveResultData)
      } else {
        let loadResult;
        try {
          loadResult = await PollsService.loadPoll(state.poll.id)
        } catch (serverError) {
          commit(ACTIONS.SET_ERROR, serverError)
          return
        }
        const loadResultData = loadResult.data;
        const saveResultData = {
          ...loadResultData,
          title: state.poll.title,
          date_begin: formatDateForServer(state.poll.dateBegin),
          date_end: formatDateForServer(state.poll.dateEnd),
          data: JSON.stringify(pollDataClearedQuestions)
        }
        doSave = PollsService.savePoll(state.poll.id, saveResultData)
      }
      try {
        await doSave
      } catch (serverError) {
        commit(ACTIONS.SET_ERROR, serverError)
        return
      }
      commit(ACTIONS.SET_POLL, null)
    },
    resetPoll ({ commit, state }) {
      commit(ACTIONS.SET_POLL, null)
    },
    createPoll ({ commit }) {
      commit(ACTIONS.CREATE_POLL, null)
    },
  },
  getters: {
    getQuestions (state) {
      return state.poll && state.poll.questions
    },
    getPollId (state) {
      return state.poll && state.poll.id
    },
    getPoll (state) {
      return state.poll
    },
    isNewPoll (state) {
      return state.isNew
    },
    isLoading (state) {
      return state.isLoading
    },
    getError (state) {
      return state.error
    },
    isPollEditorOpen (state) {
      return state.poll || state.error || state.isLoading
    }
  }
}

export default pollModule
