import Vue from 'vue'
import Vuex from 'vuex'
import PollModule from './poll_module'
import PollsModule from './polls_module'

// Make vue aware of Vuex
Vue.use(Vuex)

const modules = {
  Polls: PollsModule,
  Poll: PollModule
}

// Combine the initial state and the mutations to create a Vuex store.
// This store can be linked to our app.
export default new Vuex.Store({
  modules
})
