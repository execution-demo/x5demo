// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import filter from './utils/filter'
import store from './store'
import Octicon from 'vue-octicon/components/Octicon.vue'
import 'vue-octicon/icons'
import './assets/css/style.css'

Vue.config.productionTip = false

Vue.component('octicon', Octicon)

/* eslint-disable no-new */
new Vue({
  render: h => h(App),
  filter,
  router,
  store
}).$mount('#app')
