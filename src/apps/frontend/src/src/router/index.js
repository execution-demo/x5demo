import Vue from 'vue'
import Router from 'vue-router'
import AppPage from '@/views/AppPage/AppPage'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'questions', component: AppPage }
  ]
})
