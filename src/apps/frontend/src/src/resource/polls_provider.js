import HttpRequest from './http_request'

class PollsProvider extends HttpRequest {
  getPolls () {
    return this.fetch('polls/')
  }
  loadPoll (id) {
    return this.fetch(`polls/${id}/`)
  }
  savePoll (id, data) {
    return this.update(`polls/${id}/`, data)
  }
  createPoll (data) {
    return this.create(`polls/`, data)
  }
}

export default PollsProvider
