export function formatDate(date) {
  return date.toLocaleDateString(['ru'], { year: 'numeric', month: 'numeric', day: 'numeric' })
}

export function formatDateRange(dateBegin, dateEnd) {
  if (!dateBegin) {
    if (!dateEnd) {
      return '';
    }
    return `- ${formatDate(dateEnd)}`
  }
  if (!dateEnd) {
    return formatDate(dateBegin)
  }
  return `${formatDate(dateBegin)} - ${formatDate(dateEnd)}`;
}

export function formatDateForServer(date) {
  if (!date) {
    return null;
  }
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
}

export function parseDateFromServer(date) {
  if (!date) {
    return null;
  }
  return new Date(date);
}
