export class ResponseRecognitionError extends Error {
  constructor (error) {
    super()
    this.responseRecognitionError = error;
  }
}
