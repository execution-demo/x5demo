export function makeHashById (arr) {
  return arr.reduce(
    (hash, item) => Object.assign(hash,
      {
        [item.id]: item
      }),
    Object.create(null))
}
