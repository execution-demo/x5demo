from rest_framework.routers import DefaultRouter

from apps.poll.views import PollViewSet


router = DefaultRouter()
router.register(r'polls', PollViewSet, basename='user')

urlpatterns = router.urls
