from rest_framework import serializers

from apps.poll.models import Poll


class PollSerializer(serializers.ModelSerializer):

    class Meta:
        model = Poll
        fields = [
            'id',
            'title',
            'created_at',
            'description',
            'data',
            'date_begin',
            'date_end',
        ]
