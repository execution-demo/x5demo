from rest_framework import viewsets

from apps.poll.models import Poll
from apps.poll.serializers import PollSerializer


class PollViewSet(viewsets.ModelViewSet):

    queryset = Poll.objects.all().order_by('-created_at')
    serializer_class = PollSerializer
