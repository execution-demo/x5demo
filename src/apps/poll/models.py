from django.contrib import admin
from django.core.exceptions import ValidationError
from django.db import models

from apps.base.models import BaseModel


class Poll(BaseModel):

    title = models.CharField('title', max_length=128)
    created_at = models.DateTimeField('created at', auto_now_add=True)
    description = models.TextField('text', max_length=4096)
    data = models.TextField('poll data', max_length=65536, blank=True)
    date_begin = models.DateField('date begin', null=True, blank=True)
    date_end = models.DateField('date end', null=True, blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.date_begin and self.date_end:
            if self.date_begin > self.date_end:
                raise ValidationError('Wrong date range')
        return super().save(*args, **kwargs)

    class Meta:
        ordering = ['-created_at']
        verbose_name = 'Опрос'
        verbose_name_plural = 'Опросы'


admin.site.register(Poll)
