"""Различные базовые модели для построения ьизнес-логики приложения."""

from apps.base.mixins import StringFieldsCleanerMixin


class BaseModel(StringFieldsCleanerMixin):

    u"""Базовый класс для всех моделей системы."""

    def __unicode__(self):
        """ Определяет текстовое представление объекта """
        name = getattr(self, 'name', None) or getattr(self, 'fullname', None)
        if name:
            if callable(name):
                name = name()
            return u'{%s: %s}' % (self.pk, name)
        else:
            return u'{%s}' % self.pk

    class Meta:
        abstract = True
