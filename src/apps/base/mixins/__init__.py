from django.db import models


class StringFieldsCleanerMixin(models.Model):

    u"""Примесь для удаления из строковых полей модели лишних пробелов.

    Во всех текстовых полях модели удаляет пробельные символы в начале и конце
    строки, несколько идущих подряд пробелов заменяет на один.

    Для полей с разрешенными пустыми значениями (null=True) пустые строки
    заменяет на None.
    """

    def clean_fields(self, exclude=None):
        for field in self._meta.fields:
            if isinstance(field, (models.TextField, models.CharField)):
                field_value = getattr(self, field.attname)

                if field_value is not None:
                    field_value = field_value.strip()
                    # Удаление лишних пробелов
                    while field_value.find(u'  ') != -1:
                        field_value = field_value.replace(u'  ', u' ')

                if not field_value:
                    field_value = None if field.null else u''

                setattr(self, field.attname, field_value)

        return super(StringFieldsCleanerMixin, self).clean_fields(exclude)

    class Meta:
        abstract = True
