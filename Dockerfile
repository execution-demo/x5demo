FROM node:lts-alpine as build-stage
WORKDIR /app
ARG X5POLL_API_PATH

ENV BASE_DIR  /app/src/apps/frontend/src/
ENV X5POLL_API_PATH $X5POLL_API_PATH
COPY ./src/apps/frontend/src/package*.json ./
RUN npm install
COPY . .

WORKDIR  $BASE_DIR
RUN npm run build


FROM ubuntu:18.04 as production-stage

# Install required packages and remove the apt packages cache when done.

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
  git \
  python3 \
  python3-dev \
  python3-setuptools \
  python3-pip \
  nginx \
  supervisor \
  sqlite3 && \
  pip3 install -U pip setuptools && \
   rm -rf /var/lib/apt/lists/*


RUN pip3 install gunicorn

# setup all the configfiles
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY docker-config/nginx-app.conf /etc/nginx/sites-available/default
COPY docker-config/supervisor-app.conf /etc/supervisor/conf.d/

RUN mkdir -p /home/docker/code/run && mkdir /home/docker/code/log/
# COPY requirements.txt and RUN pip install BEFORE adding the rest of your code, this will cause Docker's caching mechanism
# to prevent re-installing (all your) dependencies when you made a change a line or two in your app.

COPY requirements /home/docker/code/app/requirements
WORKDIR  /home/docker/code/app/requirements/
RUN pip3 install -r prod.txt


# add (the rest of) our code
COPY . /home/docker/code/

#RUN apt-get -y update && apt-get -y install mc && apt-get -y install vim

WORKDIR  /home/docker/code/src/apps/
ARG PYTHONPATH
ARG DJANGO_SETTINGS_MODULE

ENV BASE_DIR  /app/src/apps/frontend/src/
ENV PYTHONPATH $PYTHONPATH
ENV DJANGO_SETTINGS_MODULE $DJANGO_SETTINGS_MODULE

RUN python3 manage.py migrate

ENV BASE_DIR  /app/src/apps/frontend/src/
WORKDIR  $BASE_DIR
COPY --from=build-stage  $BASE_DIR/dist /usr/share/nginx/html

EXPOSE 80

ARG X5POLL_API_PATH
ARG PYTHONPATH
ARG DJANGO_SETTINGS_MODULE

ENV X5POLL_API_PATH $X5POLL_API_PATH
ENV PYTHONPATH $PYTHONPATH
ENV DJANGO_SETTINGS_MODULE $DJANGO_SETTINGS_MODULE

CMD ["supervisord", "-n"]



