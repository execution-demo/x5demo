#! coding: utf-8
import os
import datetime

current_dir = os.path.abspath(os.path.dirname(__file__))
dirs = current_dir.rsplit(os.sep, 3)
short_name = 'x5poll'
proc_dir = os.path.join(*dirs[0:-1])
pythonpath = os.path.join(proc_dir, 'src')
run_dir = os.path.join(proc_dir, 'run') # путь до pid- и сокет- файлов
bind = '127.0.0.1:8000'
proc_name = "%s/%s" % (run_dir, short_name)
workers = 4
errorlog = "%s/log/gunicorn.log" % proc_dir
loglevel = "debug"
pidfile = "%s/%s.pid" % (run_dir, short_name)
timeout = 1800
max_requests = 1000