#

clean-pyc:
	@echo Cleaning sources...
	@py3clean .

isort:
	sh -c "isort --skip-glob=.tox --recursive . "

run:
	python src/apps/manage.py runserver

createsuperuser:
	python src/apps/manage.py createsuperuser

showmigrations:
	python src/apps/manage.py showmigrations

migrate:
	python src/apps/manage.py migrate

makemigrations:
	python src/apps/manage.py makemigrations
